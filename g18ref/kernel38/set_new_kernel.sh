#! /bin/bash

cp u-boot-dt.bin ../u-boot.bin -df
cp ddr_init.bin ../ddr_init.bin -df
cp u-boot-orig.bin ../u-boot-orig.bin -df

cp Kernel_38.mk ../Kernel.mk -df
cp g18ref_38.mk ../g18ref.mk -df
rm -rf ../overlay/packages/apps/Launcher2/
rm -rf ../overlay/packages/apps/Settings/

echo "set 3.8 kernel compile enviroment done"
