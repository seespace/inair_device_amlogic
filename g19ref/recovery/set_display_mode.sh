#!/sbin/sh

until [ -f /tmp/recovery.log ]
do
  echo "No found /tmp/recovery.log !!!"
done

hpdstate=$(cat /sys/class/amhdmitx/amhdmitx0/hpd_state)
outputmode=
until [ "$outputmode" != "" ]
do
    busybox grep "ubootenv.var.outputmode" /tmp/recovery.log > /tmp/1
    outputmode=$(busybox sed 's/ubootenv.var.outputmode=//g' /tmp/1)
done
busybox grep "ro.platform.has.cvbsmode" /tmp/recovery.log > /tmp/1
hascvbs=$(busybox sed 's/ro.platform.has.cvbsmode=//g' /tmp/1)
echo hpdstate=$hpdstate outputmode=$outputmode hascvbs=$hascvbs
if [ "$hascvbs" = "true" ]; then
        busybox grep "ubootenv.var.cvbsmode" /tmp/recovery.log > /tmp/1
        cvbsmode=$(busybox sed 's/ubootenv.var.cvbsmode=//g' /tmp/1)

        if [ "$cvbsmode" = "480cvbs" ]; then
                echo null > /sys/class/display2/mode
                echo 480cvbs > /sys/class/display2/mode
                echo 1 > /sys/class/video2/screen_mode
        else
                echo null > /sys/class/display2/mode
                echo 576cvbs > /sys/class/display2/mode
                echo 1 > /sys/class/video2/screen_mode
        fi

        if [ "$outputmode" = "1080p" -o "$outputmode" = "1080i" -o "$outputmode" = "1080p50hz" -o "$outputmode" = "1080i50hz" ] ; then
            echo 960  > /sys/module/amvideo2/parameters/clone_frame_scale_width
        else
            echo 0  > /sys/module/amvideo2/parameters/clone_frame_scale_width
        fi
fi
case $outputmode in
    480cvbs)
    busybox grep "ubootenv.var.480ioutputx" /tmp/recovery.log > /tmp/1
    outputx=$(busybox sed 's/ubootenv.var.480ioutputx=//g' /tmp/1)
    if [ "$outputx" = "" ] ; then
        outputx=0
    fi
    busybox grep "ubootenv.var.480ioutputy" /tmp/recovery.log > /tmp/1
    outputy=$(busybox sed 's/ubootenv.var.480ioutputy=//g' /tmp/1)
    if [ "$outputy" = "" ] ; then
        outputy=0
    fi
    busybox grep "ubootenv.var.480ioutputheight" /tmp/recovery.log > /tmp/1
    outputwidth=$(busybox sed 's/ubootenv.var.480ioutputheight=//g' /tmp/1)
    if [ "$outputwidth" = "" ] ; then
        outputwidth=720
    fi
    busybox grep "ubootenv.var.480ioutputheight" /tmp/recovery.log > /tmp/1
    outputheight=$(busybox sed 's/ubootenv.var.480ioutputheight=//g' /tmp/1)
    if [ "$outputheight" = "" ] ; then
        outputheight=480
    fi
    ;;

    576cvbs)
    busybox grep "ubootenv.var.576ioutputx" /tmp/recovery.log > /tmp/1
    outputx=$(busybox sed 's/ubootenv.var.576ioutputx=//g' /tmp/1)
    if [ "$outputx" = "" ] ; then
        outputx=0
    fi
    busybox grep "ubootenv.var.576ioutputy" /tmp/recovery.log > /tmp/1
    outputy=$(busybox sed 's/ubootenv.var.576ioutputy=//g' /tmp/1)
    if [ "$outputy" = "" ] ; then
        outputy=0
    fi
    busybox grep "ubootenv.var.476ioutputheight" /tmp/recovery.log > /tmp/1
    outputwidth=$(busybox sed 's/ubootenv.var.576ioutputheight=//g' /tmp/1)
    if [ "$outputwidth" = "" ] ; then
        outputwidth=720
    fi
    busybox grep "ubootenv.var.576ioutputheight" /tmp/recovery.log > /tmp/1
    outputheight=$(busybox sed 's/ubootenv.var.576ioutputheight=//g' /tmp/1)
    if [ "$outputheight" = "" ] ; then
        outputheight=576
    fi
    ;;
    720p)
    busybox grep "ubootenv.var.720poutputx" /tmp/recovery.log > /tmp/1
    outputx=$(busybox sed 's/ubootenv.var.720poutputx=//g' /tmp/1)
    if [ "$outputx" = "" ] ; then
        outputx=0
    fi
    busybox grep "ubootenv.var.720poutputy" /tmp/recovery.log > /tmp/1
    outputy=$(busybox sed 's/ubootenv.var.720poutputy=//g' /tmp/1)
    if [ "$outputy" = "" ] ; then
        outputy=0
    fi  
    busybox grep "ubootenv.var.720poutputheight" /tmp/recovery.log > /tmp/1
    outputwidth=$(busybox sed 's/ubootenv.var.720poutputheight=//g' /tmp/1)
    if [ "$outputwidth" = "" ] ; then
        outputwidth=1280
    fi  
    busybox grep "ubootenv.var.720poutputheight" /tmp/recovery.log > /tmp/1
    outputheight=$(busybox sed 's/ubootenv.var.720poutputheight=//g' /tmp/1)
    if [ "$outputheight" = "" ] ; then
        outputheight=720
    fi
    ;;
    1080p)
    busybox grep "ubootenv.var.1080poutputx" /tmp/recovery.log > /tmp/1
    outputx=$(busybox sed 's/ubootenv.var.1080poutputx=//g' /tmp/1)
    if [ "$outputx" = "" ] ; then
        outputx=0
    fi
    busybox grep "ubootenv.var.1080poutputy" /tmp/recovery.log > /tmp/1
    outputy=$(busybox sed 's/ubootenv.var.1080poutputy=//g' /tmp/1)
    if [ "$outputy" = "" ] ; then
        outputy=0
    fi  
    busybox grep "ubootenv.var.1080poutputheight" /tmp/recovery.log > /tmp/1
    outputwidth=$(busybox sed 's/ubootenv.var.1080poutputheight=//g' /tmp/1)
    if [ "$outputwidth" = "" ] ; then
        outputwidth=1920
    fi  
    busybox grep "ubootenv.var.1080poutputheight" /tmp/recovery.log > /tmp/1
    outputheight=$(busybox sed 's/ubootenv.var.1080poutputheight=//g' /tmp/1)
    if [ "$outputheight" = "" ] ; then
        outputheight=1080
    fi
    ;;
    1080i)
    busybox grep "ubootenv.var.1080ioutputx" /tmp/recovery.log > /tmp/1
    outputx=$(busybox sed 's/ubootenv.var.1080ioutputx=//g' /tmp/1)
    if [ "$outputx" = "" ] ; then
        outputx=0
    fi
    busybox grep "ubootenv.var.1080ioutputy" /tmp/recovery.log > /tmp/1
    outputy=$(busybox sed 's/ubootenv.var.1080ioutputy=//g' /tmp/1)
    if [ "$outputy" = "" ] ; then
        outputy=0
    fi  
    busybox grep "ubootenv.var.1080ioutputheight" /tmp/recovery.log > /tmp/1
    outputwidth=$(busybox sed 's/ubootenv.var.1080ioutputheight=//g' /tmp/1)
    if [ "$outputwidth" = "" ] ; then
        outputwidth=1920
    fi  
    busybox grep "ubootenv.var.1080ioutputheight" /tmp/recovery.log > /tmp/1
    outputheight=$(busybox sed 's/ubootenv.var.1080ioutputheight=//g' /tmp/1)
    if [ "$outputheight" = "" ] ; then
        outputheight=1080
    fi
    ;;
    1080i50hz)
    busybox grep "ubootenv.var.1080ioutputx" /tmp/recovery.log > /tmp/1
    outputx=$(busybox sed 's/ubootenv.var.1080ioutputx=//g' /tmp/1)
    if [ "$outputx" = "" ] ; then
        outputx=0
    fi
    busybox grep "ubootenv.var.1080ioutputy" /tmp/recovery.log > /tmp/1
    outputy=$(busybox sed 's/ubootenv.var.1080ioutputy=//g' /tmp/1)
    if [ "$outputy" = "" ] ; then
        outputy=0
    fi  
    busybox grep "ubootenv.var.1080ioutputheight" /tmp/recovery.log > /tmp/1
    outputwidth=$(busybox sed 's/ubootenv.var.1080ioutputheight=//g' /tmp/1)
    if [ "$outputwidth" = "" ] ; then
        outputwidth=1920
    fi  
    busybox grep "ubootenv.var.1080ioutputheight" /tmp/recovery.log > /tmp/1
    outputheight=$(busybox sed 's/ubootenv.var.1080ioutputheight=//g' /tmp/1)
    if [ "$outputheight" = "" ] ; then
        outputheight=1080
    fi
    ;;
    1080p50hz)
    busybox grep "ubootenv.var.1080poutputx" /tmp/recovery.log > /tmp/1
    outputx=$(busybox sed 's/ubootenv.var.1080poutputx=//g' /tmp/1)
    if [ "$outputx" = "" ] ; then
        outputx=0
    fi
    busybox grep "ubootenv.var.1080poutputy" /tmp/recovery.log > /tmp/1
    outputy=$(busybox sed 's/ubootenv.var.1080poutputy=//g' /tmp/1)
    if [ "$outputy" = "" ] ; then
        outputy=0
    fi
    busybox grep "ubootenv.var.1080poutputheight" /tmp/recovery.log > /tmp/1
    outputwidth=$(busybox sed 's/ubootenv.var.1080poutputheight=//g' /tmp/1)
    if [ "$outputwidth" = "" ] ; then
        outputwidth=1920
    fi
    busybox grep "ubootenv.var.1080poutputheight" /tmp/recovery.log > /tmp/1
    outputheight=$(busybox sed 's/ubootenv.var.1080poutputheight=//g' /tmp/1)
    if [ "$outputheight" = "" ] ; then
        outputheight=1080
    fi
    ;;
    720p50hz)
    busybox grep "ubootenv.var.720poutputx" /tmp/recovery.log > /tmp/1
    outputx=$(busybox sed 's/ubootenv.var.720poutputx=//g' /tmp/1)
    if [ "$outputx" = "" ] ; then
        outputx=0
    fi
    busybox grep "ubootenv.var.720poutputy" /tmp/recovery.log > /tmp/1
    outputy=$(busybox sed 's/ubootenv.var.720poutputy=//g' /tmp/1)
    if [ "$outputy" = "" ] ; then
        outputy=0
    fi
    busybox grep "ubootenv.var.720poutputheight" /tmp/recovery.log > /tmp/1
    outputwidth=$(busybox sed 's/ubootenv.var.720poutputheight=//g' /tmp/1)
    if [ "$outputwidth" = "" ] ; then
        outputwidth=1280
    fi
    busybox grep "ubootenv.var.720poutputheight" /tmp/recovery.log > /tmp/1
    outputheight=$(busybox sed 's/ubootenv.var.720poutputheight=//g' /tmp/1)
    if [ "$outputheight" = "" ] ; then
        outputheight=720
    fi
    ;;
    *)
    outputmode=720p
    busybox grep "ubootenv.var.720poutputx" /tmp/recovery.log > /tmp/1
    outputx=$(busybox sed 's/ubootenv.var.720poutputx=//g' /tmp/1)
    if [ "$outputx" = "" ] ; then
        outputx=0
    fi
    busybox grep "ubootenv.var.720poutputy" /tmp/recovery.log > /tmp/1
    outputy=$(busybox sed 's/ubootenv.var.720poutputy=//g' /tmp/1)
    if [ "$outputy" = "" ] ; then
        outputy=0
    fi
    busybox grep "ubootenv.var.720poutputheight" /tmp/recovery.log > /tmp/1
    outputwidth=$(busybox sed 's/ubootenv.var.720poutputheight=//g' /tmp/1)
    if [ "$outputwidth" = "" ] ; then
        outputwidth=1280
    fi
    busybox grep "ubootenv.var.720poutputheight" /tmp/recovery.log > /tmp/1
    outputheight=$(busybox sed 's/ubootenv.var.720poutputheight=//g' /tmp/1)
    if [ "$outputheight" = "" ] ; then
        outputheight=720
    fi

esac
if [ "$outputmode" = "480i" -o "$outputmode" = "576i" ]; then
  if [ "$hascvbs" = "true" ] ; then
    if [ "$outputmode" = "480i" ];then
      echo 480cvbs > /sys/class/display/mode
      echo null > /sys/class/display2/mode
    else
      echo 576cvbs > /sys/class/display/mode
      echo null > /sys/class/display2/mode
    fi
  else
    echo $outputmode > /sys/class/display/mode
  fi
else
    echo $outputmode > /sys/class/display/mode                                                                                                                          
fi
echo $outputx $outputy $outputwidth $outputheight
busybox echo $outputx $outputy $(($outputwidth + $outputx - 1)) $(($outputheight + $outputy - 1)) 0 > /sys/class/ppmgr/ppscaler_rect
busybox echo 1 > /sys/class/graphics/fb0/free_scale                                                                                                                    
busybox echo 0 > /sys/class/graphics/fb0/blank

#busybox echo null > /sys/class/display2/mode
#busybox echo 576cvbs > /sys/class/display2/mode 
#busybox echo 1 > /sys/class/video2/screen_mode
#busybox echo 0  > /sys/module/amvideo2/parameters/clone_frame_scale_width
