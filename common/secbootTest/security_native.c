/*
 * author:	wei.liu@amlogic.com
 * date:	2012-08-24
 * usage: 	security support for IPTV
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "security_native.h"

#define SECURITY_DEVICE "/dev/security_device"

/*
 * return
 * 0: not exist
 * others: exist
 */
int is_in_exist()
{
	int fd = 0;
	int result = 0;

	fd = open(SECURITY_DEVICE, O_WRONLY);
	if(fd == -1)
	{
		printf("is_in_exist, open device unsuccessfully!\n");
		return -1;
	}

	result = ioctl(fd, SECURITY_IOC_G_IN_STU, 0);
	if(result != 0)
	{
		printf("in already exist!\n");
	}

	close(fd);

	return result;
}

int burn_in(char* in)
{
    int fd = 0;
    int result = 0;

    fd = open(SECURITY_DEVICE, O_WRONLY);
    if(fd == -1)
    {
        printf("burn_in, open device unsuccessfully!\n");
        return -1;
    }

    result = ioctl(fd, SECURITY_IOC_B_IN, in);
    if(result != 0)
    {
        printf("burn in unsuccessfully!\n");
    }

    close(fd);

    return result;
}

int burn_sn(char* sn)
{
    int fd = 0;
    int result = 0;

    fd = open(SECURITY_DEVICE, O_WRONLY);
    if(fd == -1)
    {
        printf("burn_sn, open device unsuccessfully!\n");
        return -1;
    }

    result = ioctl(fd, SECURITY_IOC_B_SN, sn);
    if(result != 0)
    {
        printf("burn sn unsuccessfully!\n");
    }

    close(fd);

    return result;
}

int read_sn(char* sn)
{
    int fd = 0;
    int result = 0;

    fd = open(SECURITY_DEVICE, O_RDONLY);
    if(fd == -1)
    {
        printf("read_sn, open device unsuccessfully!\n");
        return -1;
    }

    result = ioctl(fd, SECURITY_IOC_G_SN, sn);
    if(result != 0)
    {
        printf("read sn unsuccessfully!\n");
    }

    close(fd);

    return result;
}

int read_rn(char* rn)
{
    int fd = 0;
    int result = 0;

    fd = open(SECURITY_DEVICE, O_RDONLY);
    if(fd == -1)
    {
        printf("read_rn, open device unsuccessfully!\n");
        return -1;
    }

    result = ioctl(fd, SECURITY_IOC_G_RN, rn);
    if(result != 0)
    {
        printf("read rn unsuccessfully!\n");
    }

    close(fd);

    return result;
}

int decrypt_cloud_challenge(char* me, int me_length, char* cd, int* cd_length, int procedure)
{
    int fd = 0;
    int result = 0;

    char* buffer = NULL;

    int length = sizeof(security_me_t) > sizeof(security_cd_t) ? sizeof(security_me_t) : sizeof(security_cd_t);

    printf("decrypt_cloud_challenge, length is: %d\n", length);

    buffer = (char*)malloc(length);
    if(buffer == NULL)
    {
        printf("decrypt_cloud_challenge, memory malloc unsuccessfully!\n");
        return -1;
    }

    fd = open(SECURITY_DEVICE, O_RDWR);
    if(fd == -1)
    {
        printf("decrypt_cloud_challenge, open device unsuccessfully!\n");
        return -1;
    }

    result = ioctl(fd, SECURITY_IOC_S_PR, &procedure);
    if(result != 0)
    {
        printf("decrypt_cloud_challenge, set procedure unsuccessfully!\n");
        close(fd);
        return -1;
    }

    memcpy((void*)buffer, (void*)me, me_length);
    result = ioctl(fd, SECURITY_IOC_S_ME_G_CD, buffer);
    if(result != 0)
    {
        printf("decrypt_cloud_challenge, decrypt me to get cd unsuccessfully!\n");
        close(fd);
        return -1;
    }

    memcpy((void*)cd, buffer, sizeof(security_cd_t));

    close(fd);
    return 0;
}

int decrypt_terminal_challenge(char* iv, int iv_length, char* xx)
{
    int fd = 0;
    int result = 0;

    char* buffer = NULL;

    int length = sizeof(security_iv_t) >= sizeof(security_xx_t) ? sizeof(security_iv_t) : sizeof(security_xx_t);

    printf("decrypt_terminal_challenge, length is: %d\n", length);

    buffer = (char*)malloc(length);
    if(buffer == NULL)
    {
        printf("decrypt_terminal_challenge, memory malloc unsuccessfully!\n");
        return -1;
    }

    fd = open(SECURITY_DEVICE, O_RDWR);
    if(fd == -1)
    {
        printf("decrypt_terminal_challenge, open device unsuccessfully!\n");
        return -1;
    }

    memcpy((void*)buffer, (void*)iv, iv_length);
    result = ioctl(fd, SECURITY_IOC_S_IV_G_XX, buffer);
    if(result != 0)
    {
        printf("decrypt_terminal_challenge, decrypt iv to get xx unsuccessfully!\n");
        close(fd);
        return -1;
    }

    memcpy((void*)xx, buffer, sizeof(security_xx_t));

    printf("decrypt_terminal_challenge, decrypt over!");

    close(fd);
    return 0;
}

int decrypt_hashtable(char* hs, int* hs_length)
{
	int fd = 0;
	int result = 0;

	int procedure = DEC_WITH_PUB | RSA_BUF_IN_TYPE_IGNORE | RSA_BUF_OUT_TYPE_IGNORE;

	fd = open(SECURITY_DEVICE, O_RDWR);
	if(fd == -1)
	{
		printf("decrypt_hashtable, open device unsuccessfully!\n");
		return -1;
	}

	result = ioctl(fd, SECURITY_IOC_S_HS_LEN, hs_length);
	if(result != 0)
	{
		printf("decrypt_hashtable, set hashtable length unsuccessfully!\n");
		close(fd);
		return -1;
	}

	result = ioctl(fd, SECURITY_IOC_S_PR, &procedure);
	if(result != 0)
	{
		printf("decrypt_hashtable, set procedure unsuccessfully!\n");
		close(fd);
		return -1;
	}

	result = ioctl(fd, SECURITY_IOC_D_HS, hs);
	if(result != 0)
	{
		printf("decrypt_hashtable, hashtable decrypt unsuccessfully!\n");
		close(fd);
		return -1;
	}

	result = ioctl(fd, SECURITY_IOC_G_HS_LEN, hs_length);
	if(result != 0)
	{
		printf("decrypt_hashtable, get hashtable length unsuccessfully!\n");
		close(fd);
		return -1;
	}

	result = ioctl(fd, SECURITY_IOC_G_HS, hs);
	if(result != 0)
	{
		printf("decrypt_hashtable, hashtable get unsuccessfully!\n");
		close(fd);
		return -1;
	}

	close(fd);
	return 0;
}

int decrypt_2rsa_enc(char* enc_buf, int  * dec_length)
{
	int fd = 0;
	int result = 0;

	fd = open(SECURITY_DEVICE, O_RDWR);
	if(fd == -1)
	{
		printf("decrypt_2rsa_enc, open device unsuccessfully!\n");
		return -1;
	}
	printf("call security ioctl\n");
	result = ioctl(fd, SECURITY_IOC_S_2RSA_FILE_LEN, dec_length);
	if(result != 0)
	{
		printf("decrypt_2rsa_enc, decrypt SECURITY_IOC_S_2RSA_FILE_LEN unsuccessfully!\n");
		close(fd);
		return -1;
	}
	result = ioctl(fd, SECURITY_IOC_G_2RSA_FILE_BUF, enc_buf);
	if(result != 0)
	{
		printf("decrypt_2rsa_enc, decrypt SECURITY_IOC_G_2RSA_FILE_BUF unsuccessfully!\n");
		close(fd);
		return -1;
	}

	result = ioctl(fd, SECURITY_IOC_G_2RSA_FILE_LEN, dec_length);
	if(result != 0)
	{
		printf("decrypt_hashtable, set procedure unsuccessfully!\n");
		close(fd);
		return -1;
	}
	close(fd);
	return 0;
}


/*int main(int argc, char** argv)
{
    char* sn = NULL;
    char* rn = NULL;

    char* write_in = NULL;
    char* write_sn = NULL;

    int index = 0;

    rn = (char*)malloc(sizeof(security_rn_t));

    read_rn(rn);

    printf("rn is: %s\n", rn);

    const unsigned msg_enc[]={
             0xdd7a115c , 0x69dd8ab4 , 0xa8947f3d , 0x6f6f5702,
             0xfe27dd6c , 0x14465eb5 , 0x1b193552 , 0x243289ba,
             0xc6c9d40d , 0xcf851443 , 0x134065f0 , 0x9be0ca23,
             0x71209d6b , 0xc7ff285e , 0x579b1237 , 0xc6ce55c5,
             0x22a6b2f0 , 0x2aab2c99 , 0x106a253e , 0x89e57941,
             0x9e2fafd3 , 0x532248b6 , 0x8cd7f1ad , 0x09975cb0,
             0xe3e69873 , 0x1cecde1a , 0x2ff90da4 , 0x413ff398,
             0xf8f037ad , 0x3255616f , 0x33933870 , 0x0d95941b,
             0x00000000 , 0x00000000 , 0x00000000 , 0x00000000
    };

    char cd[64];
    int cd_length = 0;

    decrypt_cloud_challenge((char*)msg_enc, 128, cd, &cd_length,
        // DEC_WITH_PUB | RSA_BUF_IN_TYPE_LITTLE | RSA_BUF_OUT_TYPE_LITTLE);
        DEC_WITH_PUB | RSA_BUF_IN_TYPE_IGNORE | RSA_BUF_OUT_TYPE_IGNORE);

    // printf("cd is: %s\n", cd);

    for(index = 0; index < 64; index++)
    {
        printf("%d, ", cd[index]);
    }
    printf("\n");

    for(index = 0; index < 64; index++)
    {
        printf("%c, ", cd[index]);
    }
    printf("\n");

    sn = (char*)malloc(sizeof(security_sn_t));

    read_sn(sn);

    //printf("sn is: %s\n", sn);

    for(index = 0; index < sizeof(security_sn_t); index++)
    {
    	printf("%x, ", sn[index]);
    }

    printf("\n");



    return 0;
}*/
