/*
 * author:	wei.liu@amlogic.com
 * date:	2012-08-01
 * usage: 	security support for IPTV
 */ 

#ifndef __SECURITY_H
#define __SECURITY_H

typedef unsigned char u8;

typedef struct security_ts_s { // 时间戳, TS (TimeStamp), 14 bytes, yyyymmddhhmmss
  u8 data[14];
} security_ts_t;

typedef struct security_rn_s { // 随机数, RN (RandomNumber), 16 bytes
  u8 data[16];
} security_rn_t;


typedef struct security_sn_s { // SN, 24 bytes
  u8 factory_id[4];            // 厂商代码
  u8 product_model[4];         // 终端型号
  u8 product_batch[8];         // 产品批次
  u8 product_sn[8];            // 产品序号
} security_sn_t;

typedef struct security_me_s { // 云端挑战字, ME, 128 bytes
  u8 data[128];                // RSA1024解密算法中的Block 大小
} security_me_t;

typedef struct security_oi_s { // 运营商标识, OI (OperatorID), 4 bytes
  u8 data[4];
} security_oi_t;

typedef struct security_cd_s { // 云端挑战字明文, CD, 64 bytes
  security_oi_t oi;            // 运营商标识
  security_ts_t t1;            // 终端时间戳
  security_rn_t r1;            // 终端随机数
  security_ts_t t2;            // 云端时间戳
  security_rn_t r2;            // 云端随机数
} security_cd_t;

typedef struct security_iv_s { // 终端挑战字引文, IV, 34 bytes
  security_oi_t oi;            // 运营商标识
  security_ts_t t2;            // 云端时间戳
  security_rn_t r2;            // 云端随机数
} security_iv_t;

typedef struct security_xx_s { // 终端挑战字, XX, 32 bytes
  u8 data[32];
} security_xx_t;

typedef struct security_in_s { // IN, 24 bytes
  u8 factory_id[4];            // 厂商代码
  u8 chip_batch[8];            // 芯片批次
  u8 chip_sn[12];              // 芯片序号
} security_in_t;


#define SECURITY_DEVICE_NODE_NAME  "security"
#define SECURITY_DEVICE_FILE_NAME  "security_device"
#define SECURITY_DEVICE_PROC_NAME  "security"
#define SECURITY_DEVICE_CLASS_NAME "security_class"

#define SECURITY_SN_LENGTH 24
#define SECURITY_RN_LENGTH 16
#define SECURITY_ME_LENGTH 128
#define SECURITY_CD_LENGTH 64
#define SECURITY_IV_LENGTH 34
#define SECURITY_XX_LENGTH 32
#define SECURITY_TS_LENGTH 14
#define SECURITY_OI_LENGTH 4

typedef int(*function)(char *pSRC, int nDataLen,char *pOUT,int *pOUTLen,int procedure, int cmd);

#endif // __SECURITY_H

