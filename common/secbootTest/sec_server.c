#include "sec_server.h"
#include "mtdf.h"
#include "security_native.h"
#include "Sha256Calc.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <sys/reboot.h>
#include <cutils/android_reboot.h>

/*free buffer*/
void FreeBuf(SZ_CHAR* buffer){
	if(!buffer){		
		free(buffer);
		buffer=NULL;
	}
}

int get_read_addr(int fd,int read_offset){
	int bz=block_sizes(fd);
	if(bz<0)
		return -1;
	int end_offset=(BLOCK_SIZE/bz)*read_offset;
	if(DEBUG)ALOGI("block size in nand is %d",bz);
	int start_offset=0;
	int bad_block=0;
	while((bad_block=is_bad(fd,start_offset,end_offset))>0)
	{
		start_offset=end_offset;
		end_offset+=bad_block;
		if(DEBUG)ALOGI("start offset is %d,end offset is %d",start_offset,end_offset);
	}
	 if(bad_block==0)
		return end_offset;
	 
	 return -1;
}
/*
 * parameter:
 * content[out]: 	      read content * 
 * count[in]: in for byte count to read
 *
 * return:
 * 0: successful
 * others: unsuccessful
 *
 *function: read the hashtable in the hashtable mtd 
 *and store it in the content buffer
 */
 
int read_hashtable(SZ_CHAR* content, int count)
{	
	char mtdname[MTD_NAME_LEN];
	memset(mtdname,0,MTD_NAME_LEN);
	strncpy(mtdname,"hashtable",strlen("hashtable")+1);
	int retV=getMtdPath(mtdname);
	if(retV<0) return -1;	
	int fd = 0;	
	if(DEBUG)	ALOGI("mtdname is %s",mtdname);
	fd = open(mtdname, O_RDONLY);
	if(fd == -1)
	{
		ALOGE("read hashtable, open device %s unsuccessfully!\n", mtdname);
		return -1;
	}
	
	int readbytes = read(fd, content, count);
	if(readbytes!=count){
		ALOGE("readbytes is %d count is %d",readbytes,count);
		close(fd);
		return -1;
		}
	close(fd);
	return 0;
}
/*
 * parameter:
 * 				NULL
 *
 * return:
 *				NULL
 *function:
 *				dealing function when error ,default operation is reboot 
 */

void errDeal(){
	//android_reboot(ANDROID_RB_POWEROFF,0,0);	
	 __reboot(LINUX_REBOOT_MAGIC1, LINUX_REBOOT_MAGIC2,LINUX_REBOOT_CMD_RESTART2, "charging_reboot");
	if(DEBUG)  ALOGE("reboot the system");
	exit(1);
}
/*
 * parameter:
 * Sha256Calc[in]: 	the value to compare which got from block
 * buffer[in]: 	      the block which in the hashtable
 * 
 *function:		compare the asked record in the hashtable and mtd block 
 *
 * return:
 * 0: successful
 * others: unsuccessful
 */

int cmpHash(Sha256Calc sha256,SZ_CHAR *buffer){
	int i;
	for(i=0;i<32;i++)
	{
		if(DEBUG) ALOGE("sha256.Value[i] is %x, sha256.Value[i] is %x",(unsigned)sha256.Value[i],(unsigned)buffer[i]);
		if((sha256.Value[i])!=buffer[i])
			return -1;
	}
	return 0;
}

/*
 * parameter:
 * path[in]: 			the mtd device to open
 * block_query[in]: 	the block queried to read
 * buffer[in]: 		in for restore byte to read
 * hashbuffer[in]:		which store the hashtable 		
 *
 *function:		 	count the mtd block hash value and 
 *                        compare it with record in the hashtable
 *
 * return:
 * 0: successful
 * others: unsuccessful
 */

//int sha256_buf(int fd,int block_query,SZ_CHAR *buffer,SZ_CHAR *hashbuf)
int hash_calc(int fd,int nand_query,SZ_CHAR *buffer,SZ_CHAR *hashref)

{
		int retVal=-1;
		int read_block=0;
		int read_bytes=0;
   		int setSeek=0;
		int read_offset=0;
		int nand_read=nand_query;		
		int bz=block_sizes(fd);
		if(bz<0)
			return -1;
		/*if(block_query!=0){
			nand_read=get_read_addr(fd,block_query);	
			if(nand_read<0)
				return -1;			
		}*/
		memset(buffer,0,BLOCK_SIZE);
		while(read_bytes!=BLOCK_SIZE)
			{
				if(DEBUG) ALOGE("the nand_num %d is query",nand_read);
				int ret=self_bad(fd,nand_read);
			
				nand_read+=ret;			
				if(DEBUG) ALOGE("after query the nand_num %d",nand_read);
				setSeek=lseek(fd,nand_read*bz,SEEK_SET);
				if(setSeek==-1) return -1;			
				read_offset=read(fd,buffer+read_bytes,bz);
			
				if(read_offset!=bz)
					break;
				read_bytes+=read_offset;
				nand_read++;				
			}
		
		if(DEBUG) ALOGE("readbytes is:%d\n",read_bytes);

		if(read_bytes!=BLOCK_SIZE)
			return -2;
				
		Sha256Calc sha256;
	    Sha256Calc_reset(&sha256);
	    Sha256Calc_calculate(&sha256,buffer,read_bytes);		
				
		retVal=cmpHash(sha256,hashref);
		
	    Sha256Calc_uninit(&sha256 );	
		return retVal;
}

int cmpRef(SZ_CHAR *buffer,int system_len,int fd)
{
	Sha256Calc sha256;
	int query_nand_block;
	int bad_block=0;
	int start_offset=0;
	int ret=0;
	int i;
	int nand_bz=block_sizes(fd);
		if(nand_bz<0)
			return -1;
			
	SZ_CHAR hash_ref[BUFFER_SIZE];
	memset(hash_ref,0,BUFFER_SIZE);
	
	memset(buffer,0xffffffff,BLOCK_SIZE);	
	Sha256Calc_reset(&sha256);
	Sha256Calc_calculate(&sha256,buffer,BLOCK_SIZE);
	memcpy(hash_ref,sha256.Value,BUFFER_SIZE);
	Sha256Calc_uninit(&sha256 );	
	
	query_nand_block=system_len/nand_bz;
	if((system_len%nand_bz)!=0)
					query_nand_block+=1;
	
	
	while((bad_block=is_bad(fd,start_offset,query_nand_block))>0)
	{
		start_offset=query_nand_block;
		query_nand_block+=bad_block;
		if(DEBUG)ALOGI("last nand_block offset is %d,end offset is %d",start_offset,query_nand_block);
	}
	if(DEBUG)ALOGI("last nand_block is %d",query_nand_block);
	for(i=0;i<2;i++){
		ret=hash_calc(fd,query_nand_block,buffer,hash_ref);
		if(ret!=0) return -1;		
	}
	return 0;
}
int cmpFile(SZ_CHAR *buffer,SZ_CHAR *hashbuffer,int fd,int system_len)
{
	int nand_query;	
	int block_query;
	int ret;	
	int bqt;
	SZ_CHAR hash_ref[BUFFER_SIZE];
	int block_num;
	block_num=system_len/BLOCK_SIZE;	

//add one block check for bestv iptv sync to sha256.exe
	if(system_len%BLOCK_SIZE!=0)
		block_num+=1;
	if(DEBUG) ALOGI("block_num is %d",block_num);
	srand((unsigned)time(0));
/*	for(bqt=0;bqt<=(block_num )&&buffer!=NULL;bqt++)	
	//for(bqt=0;bqt<block_num&&buffer!=NULL;bqt++)	
	{		
		block_query=bqt;
		if(block_query!=0){
			nand_query=get_read_addr(fd,block_query);	
			if(nand_query<0)
				return -1;			
		}else{
			nand_query=block_query;
		}
		if(DEBUG) ALOGI("the no.%d is queried",block_query);	
		memset(hash_ref,0,BUFFER_SIZE);	
		memcpy(hash_ref,(hashbuffer+(block_query+2)*BUFFER_SIZE),BUFFER_SIZE);

		if((hash_calc(fd,nand_query,buffer,hash_ref))<0)	
		{
				return -1;				
		}
		
	} 	*/		
	for(bqt=0;bqt<=(block_num/RANDOMNUM)&&buffer!=NULL;bqt++)	
	//for(bqt=0;bqt<block_num&&buffer!=NULL;bqt++)	
	{	
		sleep(10);		
		block_query=(rand()%block_num);		
		if(block_query!=0){
			nand_query=get_read_addr(fd,block_query);	
			if(nand_query<0)
				return -1;			
		}else{
			nand_query=block_query;
		}
		if(DEBUG) ALOGI("the no.%d is queried",block_query);	
		memset(hash_ref,0,BUFFER_SIZE);	
		memcpy(hash_ref,(hashbuffer+(block_query+2)*BUFFER_SIZE),BUFFER_SIZE);

		if((hash_calc(fd,nand_query,buffer,hash_ref))<0)	
		{
				return -1;				
		}
		
	} 		
		return 0;
}



/*
 * parameter: 	void	
 *
 * return:
 * 0: successful
 * others: unsuccessful
 *
 * 
 */
int main(int argc,char*argv[])
{	
	char mtdname[MTD_NAME_LEN];
	SZ_CHAR buf_size[BUF_LEN_SIZE];
	SZ_CHAR *buffer=NULL;
	SZ_CHAR *hashbuf=NULL;
	int secfile_size=0;	
	int system_len=0;
	
	memset(mtdname,0,MTD_NAME_LEN);
	strncpy(mtdname,"system",strlen("system")+1);	
	int retV=getMtdPath(mtdname);
	if(retV<0) errDeal();	
	if(DEBUG) ALOGI("mtdname is %s",mtdname);		
	
	buffer=(SZ_CHAR*)malloc(BLOCK_SIZE);
	if(!buffer) errDeal();	
	if(read_hashtable(buf_size,BUF_LEN_SIZE)<0)
	{
		FreeBuf(buffer);
		errDeal();
	}	
	
	secfile_size=strtol(buf_size,NULL,16);	
	if(secfile_size<=0&&secfile_size%SEC_BLOCK!=0)
	{
		FreeBuf(buffer);
		errDeal();
	}
	if(DEBUG) ALOGE("the hash file_size is: %d\n",secfile_size);

	secfile_size+=BUFFER_SIZE;		
	hashbuf=(SZ_CHAR*)malloc(secfile_size*sizeof(SZ_CHAR));	
	if(!hashbuf)
	{	
		FreeBuf(buffer);
		errDeal();	
	}
	
	memset(hashbuf,0,secfile_size*sizeof(SZ_CHAR));	
	if(read_hashtable(hashbuf,secfile_size*sizeof(SZ_CHAR))<0)
	{
		if(DEBUG) ALOGI("readhash error in main");
		FreeBuf(buffer);
		FreeBuf(hashbuf);
		errDeal();
	}		
	
	//query kernel 
	secfile_size-=BUFFER_SIZE;
	if(decrypt_hashtable(hashbuf+BUFFER_SIZE, &secfile_size)<0)
	{			
		if(DEBUG) ALOGE("decrypt_hashtable error");
		FreeBuf(buffer);
		FreeBuf(hashbuf);
		errDeal();
	}
	
	
	memcpy(buf_size,hashbuf+BUFFER_SIZE,BUFFER_SIZE);
	system_len=strtol(buf_size,NULL,16);	
	if(DEBUG) ALOGE("the system.img length is %d",system_len);
	if(system_len<=0) 
	{
		FreeBuf(buffer);
		FreeBuf(hashbuf);
		errDeal();
	}
	system_len=system_len*1024;
	
	
	int fd = open(mtdname,O_RDONLY,00400);		
	if(fd==-1) 
	{
		FreeBuf(buffer);
		FreeBuf(hashbuf);
		errDeal();	
	}
	
	if(buffer==NULL||cmpRef(buffer,system_len,fd)<0)
	{	
		FreeBuf(hashbuf);				
		FreeBuf(buffer);				
		close(fd);
		if(DEBUG) ALOGE("error  checked");
		errDeal();
	}
	
	if(buffer==NULL||cmpFile(buffer,hashbuf,fd,system_len)<0)
	{	
		FreeBuf(hashbuf);				
		FreeBuf(buffer);				
		close(fd);
		if(DEBUG) ALOGE("error  checked");
		errDeal();
	}
			
	FreeBuf(hashbuf);
	close(fd);
	FreeBuf(buffer);
	pause();
    return 0;
}
