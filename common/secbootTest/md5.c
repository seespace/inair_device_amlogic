/*  

============================================================================  

Name        : MD5.c  

Author      : hehao  mail:peter.hehao@gmail.com  

Version     : 0.1  

Description : ????????????????????????MD5???????????win7+sp1 & cygwin(???????linux)  

============================================================================  

*/ 

#include <stdio.h>  

#include <string.h>  

#include <stdlib.h>  

#include <fcntl.h>  

#include <unistd.h>  

#include <sys/stat.h>  

#include <sys/types.h>  

#include <dirent.h>  

#include <string.h>  

#include <time.h>  

#include <utils/Log.h>

#include "mtdf.h"

#include "SizeDef.h"
#include "sec_server.h"


#define F(x, y, z) (((x) & (y)) | ((~x) & (z)))  

#define G(x, y, z) (((x) & (z)) | ((y) & (~z)))  

#define H(x, y, z) ((x) ^ (y) ^ (z))  

#define I(x, y, z) ((y) ^ ((x) | (~z)))  

   

#define RL(x, y) (((x) << (y)) | ((x) >> (32 - (y)))) //x?????????y��  

   

#define PP(x) (x<<24)|((x<<8)&0xff0000)|((x>>8)&0xff00)|(x>>24) //??x???��????,????PP(aabbccdd)=ddccbbaa  

   

#define FF(a, b, c, d, x, s, ac) a = b + (RL((a + F(b,c,d) + x + ac),s))  

#define GG(a, b, c, d, x, s, ac) a = b + (RL((a + G(b,c,d) + x + ac),s))  

#define HH(a, b, c, d, x, s, ac) a = b + (RL((a + H(b,c,d) + x + ac),s))  

#define II(a, b, c, d, x, s, ac) a = b + (RL((a + I(b,c,d) + x + ac),s))  

#define MTD_NAME_LEN		32   

unsigned A,B,C,D,a,b,c,d,i,len,flen[2],x[16];   //i???????,len?????,flen[2]?64��????????????????????  

char filename[200];   //?????  

FILE *fp,*fp_tmp;  

char paths[1000],temp_paths[1000];//��??  

int count=0;//?????????  

   

   

void md5()  

{                 //MD5??????,??64??  

   

    a=A,b=B,c=C,d=D;  

    /**//* Round 1 */ 

    FF (a, b, c, d, x[ 0], 7, 0xd76aa478); /**//* 1 */ 

    FF (d, a, b, c, x[ 1], 12, 0xe8c7b756); /**//* 2 */ 

    FF (c, d, a, b, x[ 2], 17, 0x242070db); /**//* 3 */ 

    FF (b, c, d, a, x[ 3], 22, 0xc1bdceee); /**//* 4 */ 

    FF (a, b, c, d, x[ 4], 7, 0xf57c0faf); /**//* 5 */ 

    FF (d, a, b, c, x[ 5], 12, 0x4787c62a); /**//* 6 */ 

    FF (c, d, a, b, x[ 6], 17, 0xa8304613); /**//* 7 */ 

    FF (b, c, d, a, x[ 7], 22, 0xfd469501); /**//* 8 */ 

    FF (a, b, c, d, x[ 8], 7, 0x698098d8); /**//* 9 */ 

    FF (d, a, b, c, x[ 9], 12, 0x8b44f7af); /**//* 10 */ 

    FF (c, d, a, b, x[10], 17, 0xffff5bb1); /**//* 11 */ 

    FF (b, c, d, a, x[11], 22, 0x895cd7be); /**//* 12 */ 

    FF (a, b, c, d, x[12], 7, 0x6b901122); /**//* 13 */ 

    FF (d, a, b, c, x[13], 12, 0xfd987193); /**//* 14 */ 

    FF (c, d, a, b, x[14], 17, 0xa679438e); /**//* 15 */ 

    FF (b, c, d, a, x[15], 22, 0x49b40821); /**//* 16 */ 

   

    /**//* Round 2 */ 

    GG (a, b, c, d, x[ 1], 5, 0xf61e2562); /**//* 17 */ 

    GG (d, a, b, c, x[ 6], 9, 0xc040b340); /**//* 18 */ 

    GG (c, d, a, b, x[11], 14, 0x265e5a51); /**//* 19 */ 

    GG (b, c, d, a, x[ 0], 20, 0xe9b6c7aa); /**//* 20 */ 

    GG (a, b, c, d, x[ 5], 5, 0xd62f105d); /**//* 21 */ 

    GG (d, a, b, c, x[10], 9, 0x02441453); /**//* 22 */ 

    GG (c, d, a, b, x[15], 14, 0xd8a1e681); /**//* 23 */ 

    GG (b, c, d, a, x[ 4], 20, 0xe7d3fbc8); /**//* 24 */ 

    GG (a, b, c, d, x[ 9], 5, 0x21e1cde6); /**//* 25 */ 

    GG (d, a, b, c, x[14], 9, 0xc33707d6); /**//* 26 */ 

    GG (c, d, a, b, x[ 3], 14, 0xf4d50d87); /**//* 27 */ 

    GG (b, c, d, a, x[ 8], 20, 0x455a14ed); /**//* 28 */ 

    GG (a, b, c, d, x[13], 5, 0xa9e3e905); /**//* 29 */ 

    GG (d, a, b, c, x[ 2], 9, 0xfcefa3f8); /**//* 30 */ 

    GG (c, d, a, b, x[ 7], 14, 0x676f02d9); /**//* 31 */ 

    GG (b, c, d, a, x[12], 20, 0x8d2a4c8a); /**//* 32 */ 

   

    /**//* Round 3 */ 

    HH (a, b, c, d, x[ 5], 4, 0xfffa3942); /**//* 33 */ 

    HH (d, a, b, c, x[ 8], 11, 0x8771f681); /**//* 34 */ 

    HH (c, d, a, b, x[11], 16, 0x6d9d6122); /**//* 35 */ 

    HH (b, c, d, a, x[14], 23, 0xfde5380c); /**//* 36 */ 

    HH (a, b, c, d, x[ 1], 4, 0xa4beea44); /**//* 37 */ 

    HH (d, a, b, c, x[ 4], 11, 0x4bdecfa9); /**//* 38 */ 

    HH (c, d, a, b, x[ 7], 16, 0xf6bb4b60); /**//* 39 */ 

    HH (b, c, d, a, x[10], 23, 0xbebfbc70); /**//* 40 */ 

    HH (a, b, c, d, x[13], 4, 0x289b7ec6); /**//* 41 */ 

    HH (d, a, b, c, x[ 0], 11, 0xeaa127fa); /**//* 42 */ 

    HH (c, d, a, b, x[ 3], 16, 0xd4ef3085); /**//* 43 */ 

    HH (b, c, d, a, x[ 6], 23, 0x04881d05); /**//* 44 */ 

    HH (a, b, c, d, x[ 9], 4, 0xd9d4d039); /**//* 45 */ 

    HH (d, a, b, c, x[12], 11, 0xe6db99e5); /**//* 46 */ 

    HH (c, d, a, b, x[15], 16, 0x1fa27cf8); /**//* 47 */ 

    HH (b, c, d, a, x[ 2], 23, 0xc4ac5665); /**//* 48 */ 

   

    /**//* Round 4 */ 

    II (a, b, c, d, x[ 0], 6, 0xf4292244); /**//* 49 */ 

    II (d, a, b, c, x[ 7], 10, 0x432aff97); /**//* 50 */ 

    II (c, d, a, b, x[14], 15, 0xab9423a7); /**//* 51 */ 

    II (b, c, d, a, x[ 5], 21, 0xfc93a039); /**//* 52 */ 

    II (a, b, c, d, x[12], 6, 0x655b59c3); /**//* 53 */ 

    II (d, a, b, c, x[ 3], 10, 0x8f0ccc92); /**//* 54 */ 

    II (c, d, a, b, x[10], 15, 0xffeff47d); /**//* 55 */ 

    II (b, c, d, a, x[ 1], 21, 0x85845dd1); /**//* 56 */ 

    II (a, b, c, d, x[ 8], 6, 0x6fa87e4f); /**//* 57 */ 

    II (d, a, b, c, x[15], 10, 0xfe2ce6e0); /**//* 58 */ 

    II (c, d, a, b, x[ 6], 15, 0xa3014314); /**//* 59 */ 

    II (b, c, d, a, x[13], 21, 0x4e0811a1); /**//* 60 */ 

    II (a, b, c, d, x[ 4], 6, 0xf7537e82); /**//* 61 */ 

    II (d, a, b, c, x[11], 10, 0xbd3af235); /**//* 62 */ 

    II (c, d, a, b, x[ 2], 15, 0x2ad7d2bb); /**//* 63 */ 

    II (b, c, d, a, x[ 9], 21, 0xeb86d391); /**//* 64 */ 

   

    A += a;  

    B += b;  

    C += c;  

    D += d;  

   

}  

   

   

void md5_calc(char *dir, int depth)  

{  

    DIR *dp;  

    struct dirent *entry;  

    struct stat statbuf;  

   	int n;
   
    struct dirent **namelist;

    if((dp = opendir(dir)) == NULL) {  

        ALOGE("cannot open directory: %s", dir);  

        return;  

    }  

    chdir(dir);  

		n = scandir(".", &namelist, 0, alphasort);
   	if (n < 0)
       ALOGE("scandir");
   else {
       while (n--) {
    //   printf("%s\n", namelist[n]->d_name);
       lstat(namelist[n]->d_name,&statbuf);  

        if(S_ISDIR(statbuf.st_mode)) {  

            /* Found a directory, but ignore . and .. */ 

            if(strcmp(".",namelist[n]->d_name) == 0 ||   

                strcmp("..",namelist[n]->d_name) == 0)  

                continue;  

        //    printf("%*s%s/\n",depth,"",entry->d_name);  

            /* Recurse at a new indent level */ 

            md5_calc(namelist[n]->d_name,depth+4);  

        }  

        else  

        {  

           //printf("%*s%s\n",depth,"",namelist[n]->d_name);  

           if (!(fp=fopen(namelist[n]->d_name,"rb"))) {fprintf(fp_tmp,"Can not open %s this file!\n",namelist[n]->d_name);continue;} //???????????  

                    fseek(fp, 0, SEEK_END); //?????????????��  

                           if((len=ftell(fp))==-1) {fprintf(fp_tmp,"Sorry! Can not calculate files which larger than 2 GB!\n");fclose(fp);continue;} //ftell?????long,????2GB,????????-1  

                           rewind(fp); //??????��??????  

                           A=0x67452301,B=0xefcdab89,C=0x98badcfe,D=0x10325476; //????????????  

                           flen[1]=len/0x20000000;     //flen??��??bit len?????????  

                           flen[0]=(len%0x20000000)*8;  

                           memset(x,0,64);   //?????x?????0  

                           fread(&x,4,16,fp); //??4???????,???16?????  

                           for(i=0;i<len/64;i++){    //?????????????????  

                               md5();  

                               memset(x,0,64);  

                               fread(&x,4,16,fp);  

                           }  

                           ((char*)x)[len%64]=128; //???????1,??0????,128???????10000000  

                           if(len%64>55) md5(),memset(x,0,64);  

                           memcpy(x+14,flen,8);    //????��??????????bit????  

                           md5();  

                           fclose(fp);  

                          fprintf(fp_tmp,"%s",namelist[n]->d_name);  

                          fprintf(fp_tmp,"%08x%08x%08x%08x\n",PP(A),PP(B),PP(C),PP(D)); //???��?�X???  

                           count++;//???????????  

       }
       free(namelist[n]);
       }
       free(namelist);
   }


             

 

    chdir("..");  

    closedir(dp);  

}  

/*
int md5_tmp_file(char * origin_md5)
{
		char current_md5[100];
	  fseek(fp_tmp, 0, SEEK_END); //???????????????  

    if((len=ftell(fp_tmp))==-1) {ALOGE("Sorry! Can not calculate files which larger than 2 GB!");return 0;} //ftell?????long,????2GB,????????-1  

                           rewind(fp_tmp); //??????????????  

                           A=0x67452301,B=0xefcdab89,C=0x98badcfe,D=0x10325476; //????????????  

                           flen[1]=len/0x20000000;     //flen??????bit len?????????  

                           flen[0]=(len%0x20000000)*8;  

                           memset(x,0,64);   //?????x?????0  

                           fread(&x,4,16,fp_tmp); //??4???????,???16?????  

                           for(i=0;i<len/64;i++){    //?????????????????  

                               md5();  

                               memset(x,0,64);  

                               fread(&x,4,16,fp_tmp);  

                           }  

                           ((char*)x)[len%64]=128; //???????1,??0????,128???????10000000  

                           if(len%64>55) md5(),memset(x,0,64);  

                           memcpy(x+14,flen,8);    //????????????????bit????  

                           md5();  
                           
                           ALOGE("%08x%08x%08x%08x",PP(A),PP(B),PP(C),PP(D)); //???????X???  
                           
                          memset(current_md5,0,sizeof(current_md5));
  
                          sprintf(current_md5,"%08x%08x%08x%08x",PP(A),PP(B),PP(C),PP(D));

						  ALOGE("origin_md5 is %s",origin_md5);
                          if(memcmp(current_md5,origin_md5,32)==0)
                           	return 1;//success
	return 0;
}
*/

int origin_md5_cmp(char *origin_md5)
{
	int result;
	fseek(fp_tmp, 0, SEEK_END); //???????????????  
    if((len=ftell(fp_tmp))==-1) 
    	{
    	ALOGE("Sorry! Can not calculate files which larger than 2 GB!");
		return 0;
		} 
	if(len>BUF_LEN_SIZE)
		{
		ALOGE("md5 file is too big ,cmp failed\n");
		return 0;	
		}
	rewind(fp_tmp); 
	char *dec_md5_buf=(char *)malloc(len);
	if(!dec_md5_buf)
		{
		ALOGE("malloc failed ,size is %d \n",len);
		return 0;
		}
	if((result=fread(dec_md5_buf,len,1,fp_tmp))==1)
		{		
			if(!memcmp(dec_md5_buf,origin_md5,len))
			{
				free(dec_md5_buf);
				return 1;
			}
		}	
	free(dec_md5_buf);
	return 0;
}

int md5_cal_cmp(char * origin_md5){  
	
		char * temp_path="/aml_md5.tmp";
		char * system_path="/system";

    struct stat sbuf;  

    char sdirect[1000];  
		
    DIR *dir_s;  
    
    int result;
  //   printf("Please input the sourse direct path and name :"); //????????  

  //   scanf("%s",sdirect);  
		
     dir_s=opendir(system_path);  

     if(dir_s==NULL)  

      {  

         ALOGE("This directory don't exist !");  

         return 0;  

      }  

      if(stat(system_path,&sbuf)!=0)  

      {  

         ALOGE("Get status error !");  

         return 0;  

      }  

   			fp_tmp=fopen(temp_path,"w+");
   			if(fp_tmp==NULL)
   				{
						ALOGE("create tmp file failed!");
						return 0;
   				}

         clock_t start=clock();//??????  

         md5_calc(system_path,0); //????????  

         clock_t finish=clock();//??????  

         fprintf(fp_tmp,"------------------------------------------------------\n");  

     //    printf("Calculation specified file MD5 Used when the %ld ms\n",(long)(finish-start)); //???????  

         fprintf(fp_tmp,"Calculated the total %d file\n",count);  //???????????? 

         //second time md5
      //   result=md5_tmp_file(origin_md5);
      	 result=origin_md5_cmp(origin_md5);
         fclose(fp_tmp);
		 system("rm /aml_md5.tmp");
		 system("sync");
 		 return result;  

} 

