#
#
#Android.mk for sec_server
#

LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
	sha256calc.c \
	mtdf.c \
	main.c \
	md5.c \
	security_native.c
LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libutils 
	
LOCAL_MODULE:= sec_test

LOCAL_MODULE_TAGS := optional


LOCAL_MODULE_PATH := $(TARGET_ROOT_OUT_SBIN)

include $(BUILD_EXECUTABLE)
