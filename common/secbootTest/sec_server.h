#ifndef _SECBOOT_SERVER
#define _SECBOOT_SERVER

#ifdef __cplusplus
extern "C"{
#endif

#define RANDOMNUM 5
#define BLOCK_SIZE       (4096*1024*sizeof(SZ_CHAR))
#define BUFFER_SIZE      (sizeof(SZ_CHAR)*32)
#define MTD_NAME_LEN		32	
#define BUF_LEN_SIZE		(1024*1024*sizeof(SZ_CHAR))
#define SEC_BLOCK			128


#ifdef __cplusplus
}
#endif
 
#endif/*_SECBOOT_SERVER*/
