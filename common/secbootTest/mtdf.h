#ifndef _SECBOOT_MTDID
#define _SECBOOT_MTDID

#ifdef __cplusplus
extern "C"{
#endif


#include <cutils/log.h>
#define LOG_TAG "sec_server"
int getMtdPath(char *path);
unsigned int block_sizes(int fd);
int is_bad(int mtd_fd,int start_offset,int end_offset);
int self_bad(int fd,int nand_offset);
#define DEBUG				1

#ifdef __cplusplus
}
#endif
 
#endif/*_SECBOOT_MTDID*/
