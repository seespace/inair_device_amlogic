#include "mtdf.h"
#include <unistd.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <mtd/mtd-user.h>

#define MTDERR  -3


/*
 * parameter:
 * name[in]: 			the string which mtd to asked
 * 
 *function:      return the serial number of asked mtd
 *
 * return:
 * <0: unsuccessful
 * others: mtd number
 */

int getMtdId(char *name){
		int r=-1;
		int fd;
		char buf[1024];
		char *pmtdbufp;
  		ssize_t pmtdsize;

		fd=open("/proc/mtd",O_RDONLY);
		if(fd<0) return MTDERR;
			
		buf[sizeof(buf) - 1] = '\0';

		pmtdsize = read(fd, buf, sizeof(buf) - 1);
		pmtdbufp = buf;

		while (pmtdsize > 0) {
        int mtdnum, mtdsize, mtderasesize;
        char mtdname[16];
        mtdname[0] = '\0';
        mtdnum = -1;
        r = sscanf(pmtdbufp, "mtd%d: %x %x %15s",
                   &mtdnum, &mtdsize, &mtderasesize, mtdname);
        if ((r == 4) && (mtdname[0] == '"')) {
            char *x = strchr(mtdname + 1, '"');
            if (x) {
                *x = 0;
            }            
            if(!strcmp(mtdname+1,name)){
            		close(fd);
            		return mtdnum;
            	}
        }
        while (pmtdsize > 0 && *pmtdbufp != '\n') {
            pmtdbufp++;
            pmtdsize--;
        }
        if (pmtdsize > 0) {
            pmtdbufp++;
            pmtdsize--;
        }
    }
    close(fd);
    return MTDERR;
}

int getEmmcPath(char *partition)
{	
	char emmcdevname[32];
	int fd;
	sprintf(emmcdevname, "/dev/block/%s", partition);
	fd=open(emmcdevname, O_RDWR);
	if(fd<0)
		return -1;
	close(fd);
	memcpy(partition,emmcdevname,sizeof(emmcdevname));
	return 0;
}
int getMtdPath(char *path){
	
	
	int mtdID=getMtdId(path);
	int path_len=14;
	int mtdid_t=mtdID;
	if(mtdID<0){
		return getEmmcPath(path);
		}	
	while((mtdid_t=mtdid_t/10)!=0) path_len++;
	snprintf(path,path_len,"/dev/mtd/mtd%d",mtdID);
	path[path_len-1]='\0';

	return 0;
	
}	
unsigned int block_sizes(int fd)
{
	struct mtd_info_user mtd_info;
    int ret = ioctl(fd, MEMGETINFO, &mtd_info);
	if(ret<0) return -1;
	return mtd_info.erasesize;
}
int is_bad(int mtd_fd,int start_offset,int end_offset)
{
	unsigned int bz=block_sizes(mtd_fd);	
	int invalid_block=0;
	int i;
	loff_t pos;	
	for(i=start_offset;i<end_offset;i++)
	{
		pos=lseek(mtd_fd,i*bz,SEEK_SET);
		int ret=ioctl(mtd_fd,MEMGETBADBLOCK,&pos);
		if(ret)
		{
			invalid_block++;
			continue;
		}
		
	}
	return invalid_block;
}

int self_bad(int fd,int nand_offset)
{
	unsigned int bz=block_sizes(fd);
	int invalid_block=0;
	if(DEBUG) ALOGE("in the is_bad self_bad %d",bz);
	loff_t pos;	
	while(1){	
		pos=lseek(fd,nand_offset*bz,SEEK_SET);
		int ret=ioctl(fd,MEMGETBADBLOCK,&pos);		
		if(ret)
		{
			invalid_block++;
			nand_offset++;
		}else
		{
			break;
		}
	}
	return invalid_block;
}
