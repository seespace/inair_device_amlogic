/*
 * author:	wei.liu@amlogic.com
 * date:	2012-08-24
 * usage: 	security support for IPTV
 */

#include <sys/ioctl.h>

#include "security.h"

#define SECURITY_IOC_MAGIC 'S'

#define SECURITY_IOC_G_SN       _IOR(SECURITY_IOC_MAGIC, 0x01, security_sn_t)
#define SECURITY_IOC_G_RN       _IOR(SECURITY_IOC_MAGIC, 0x02, security_rn_t)
// #define SECURITY_IOC_S_ME    _IOW(SECURITY_IOC_MAGIC, 0x03, security_me_t)
// #define SECURITY_IOC_G_CD    _IOR(SECURITY_IOC_MAGIC, 0x04, security_cd_t)
// #define SECURITY_IOC_S_IV    _IOW(SECURITY_IOC_MAGIC, 0x05, security_iv_t)
// #define SECURITY_IOC_G_XX    _IOR(SECURITY_IOC_MAGIC, 0x06, security_xx_t)
#define SECURITY_IOC_S_ME_G_CD  _IOWR(SECURITY_IOC_MAGIC, 0x03, security_me_t)
#define SECURITY_IOC_S_IV_G_XX  _IOWR(SECURITY_IOC_MAGIC, 0x05, security_iv_t)
#define SECURITY_IOC_B_SN       _IOW(SECURITY_IOC_MAGIC, 0x07, security_sn_t)
#define SECURITY_IOC_B_IN       _IOW(SECURITY_IOC_MAGIC, 0x08, security_in_t)
//#define SECURITY_IOC_U_RA     _IOW(SECURITY_IOC_MAGIC, 0x09, security_ra_t)
#define SECURITY_IOC_S_PR       _IOW(SECURITY_IOC_MAGIC, 0x0A, signed int) // set procedure
#define SECURITY_IOC_D_HS       _IOW(SECURITY_IOC_MAGIC, 0x0B, signed int) // decrypt hash table
#define SECURITY_IOC_G_HS       _IOR(SECURITY_IOC_MAGIC, 0x0C, signed int)
#define SECURITY_IOC_S_HS_LEN   _IOW(SECURITY_IOC_MAGIC, 0x0D, signed int) // set hashtable legnth
#define SECURITY_IOC_G_HS_LEN   _IOR(SECURITY_IOC_MAGIC, 0x0E, signed int)

#define SECURITY_IOC_S_ME_LEN   _IOW(SECURITY_IOC_MAGIC, 0x0F, signed int)
#define SECURITY_IOC_G_CD_LEN   _IOR(SECURITY_IOC_MAGIC, 0x10, signed int)
#define SECURITY_IOC_S_IV_LEN   _IOW(SECURITY_IOC_MAGIC, 0x11, signed int)
#define SECURITY_IOC_G_XX_LEN   _IOR(SECURITY_IOC_MAGIC, 0x12, signed int)
#define SECURITY_IOC_G_IN_STU   _IOR(SECURITY_IOC_MAGIC, 0x13, signed int) // get in status

#define SECURITY_IOC_G_2RSA_FILE_LEN	_IOWR(SECURITY_IOC_MAGIC, 0x1C, signed int)
#define SECURITY_IOC_G_2RSA_FILE_BUF	_IOWR(SECURITY_IOC_MAGIC, 0x1D, signed int)
#define SECURITY_IOC_S_2RSA_FILE_LEN	_IOWR(SECURITY_IOC_MAGIC, 0x1E, signed int)



/* int procedure bit definition*/
#define RSA_ENC_DEC_MASK (0x0F)
#define ENC_WITH_PUB	 (0)
#define ENC_WITH_PRIV	 (1)
#define DEC_WITH_PRIV  	 (3)
#define DEC_WITH_PUB   	 (2)

#define RSA_BUF_IN_TYPE_MASK    (0x30)
#define RSA_BUF_IN_TYPE_LITTLE  (0x00)
#define RSA_BUF_IN_TYPE_BIG     (0x10)
#define RSA_BUF_IN_TYPE_IGNORE (0x30)
#define RSA_BUF_OUT_TYPE_MASK    (0xC0)
#define RSA_BUF_OUT_TYPE_LITTLE  (0x00)
#define RSA_BUF_OUT_TYPE_BIG     (0x80)
#define RSA_BUF_OUT_TYPE_IGNORE (0xC0)


/*
 * parameter
 * in[in]: in to be burn;
 *
 * return
 * 0: successfully;
 * others: unsuccessfully;
 */
int burn_in(char* in);

/*
 * usage
 * decide whether the chip has a in already or not;
 *
 * return
 * 0: not exist;
 * others: already exist;
 */
int is_in_exist();

/*
 * parameter
 * sn[in]: sn to be burn;
 *
 * return
 * 0: successfully;
 * others: unsuccessfully;
 */
int burn_sn(char* sn);

/*
 * parameter
 * sn[out]: sn to be read;
 *
 * return
 * 0: successfully;
 * others: unsuccessfully;
 */
int read_sn(char* sn);

/*
 * parameter
 * rn[out]: rn to be read;
 *
 * return
 * 0: successfully;
 * others: unsuccessfully;
 */
int read_rn(char* rn);

/*
 *	parameter
 *	me[in]: me to be decrypted;
 *	me_length[in]: length of me;
 *	cd[out]: cd decrypted from me;
 *	cd_length[out]: length of cd;
 *	procedure[in]: indicate enc or dec, and the endian;
 *
 *	return
 *	0: successfully;
 *	others: unsuccessfully;
 */
int decrypt_cloud_challenge(char* me, int me_length, char* cd, int* cd_length, int procedure);

/*
 * parameter
 * iv[in]: iv to be decrypted;
 * iv_length[in]: length of iv;
 * xx[out]: xx decrypted from iv;
 *
 * return
 * 0: successfully;
 * others: unsuccessfully;
 */
int decrypt_terminal_challenge(char* iv, int iv_length, char* xx);

/*
 * parameter
 * hs[in]: encrypted hashtable content to be decrypted;
 * hs_length[in]: length of encrypted hashtable content;
 *
 * hs[out]: decrypted hashtable content;
 * hs_length[out]: length of decrypted hashtable content;
 *
 * return
 * 0:successfully;
 * others: unsuccessfully;
 */
int decrypt_hashtable(char* hs, int* hs_length);

/*
 * parameter
 * enc_buf[in]: encrypted 2rsa  content to be decrypted;
 * dec_length[in]: length after decrypted;
 *
 *
 * return
 * 0:successfully;
 * others: unsuccessfully;
 */

int decrypt_2rsa_enc(char* enc_buf, int* dec_length);


