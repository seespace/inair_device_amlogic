#!/bin/bash

#if [ -z ${TARGET_PRODUCT} ]; then
	echo "Please set andorid lunch,ex: lunch stvm8"
	source build/envsetup.sh
	lunch stvm8b-user
#fi	

PRODUCT_ROOT=`pwd`
echo $PRODUCT_ROOT
PRODUCT_OUT=out/target/product/${TARGET_PRODUCT}
PRODUCT_DEVICE=device/amlogic/${TARGET_PRODUCT}
HOST_BIN=out/host/linux-x86/bin
DIR=$PRODUCT_OUT/image`date +%Y%m%d`
VER1=`sed -n '1p' device/amlogic/android.log`
VER2=`sed -n '1p' device/amlogic/stvm8b/android.log`
export BUILD_ID=${VER1}${VER2}
export FILE=update_${BUILD_ID}_`date +%Y%m%d`_OS44_normal.zip
export FILE1=update_${BUILD_ID}_`date +%Y%m%d`_OS44_normal.img
export SPI_FILE=update_uboot_${TARGET_PRODUCT}_`date +"%Y%m%d"`.img
export USB_BURN_FILE=update_${BUILD_ID}_`date +%Y%m%d`_OS44_usb_burning.zip

if [ ! -d $PRODUCT_OUT ]; then
	echo "$PRODUCT_OUT is not creat! Build it..."
	mkdir -p $PRODUCT_OUT
fi

#make output dir
mkdir -p $DIR

#make uboot:
if [ "$1" = "update" ]; then
echo update uboot.
echo ############################################
echo making uboot ...
cd uboot;
./build_uboot.sh m8b_mbx_stv_config
if [ ! -f "build/u-boot.bin" ]; then 
	echo making uboot error!
	exit 1
fi
cp -rf build/u-boot.bin ../$PRODUCT_OUT/u-boot.bin
cp -rf build/u-boot-comp.bin ../$PRODUCT_OUT/u-boot-comp.bin
cp -rf build/ddr_init.bin ../$PRODUCT_OUT/ddr_init.bin
cp -rf build/u-boot.bin ../$PRODUCT_DEVICE/u-boot.bin
cp -rf build/u-boot-comp.bin ../$PRODUCT_DEVICE/u-boot-comp.bin
cp -rf build/ddr_init.bin ../$PRODUCT_DEVICE/ddr_init.bin
cd -
fi


#make dtb
if [ "$1" = "update" ]; then
echo update dtb 
echo "make normal ver1 dtb for box"
./$PRODUCT_DEVICE/build_dtb.sh meson8b_stvm8b_box_1G_v1
cp $PRODUCT_OUT/meson8b_stvm8b_box_1G_v1.dtb $PRODUCT_DEVICE/res_pack/m8b_normal_ver1
cp $PRODUCT_OUT/meson8b_stvm8b_box_1G_v1.dtb $DIR/m8b_normal_ver1
echo "make dongle ver1 dtb for dongle"
./$PRODUCT_DEVICE/build_dtb.sh meson8b_stvm8b_dongle_1G_v1
cp $PRODUCT_OUT/meson8b_stvm8b_dongle_1G_v1.dtb $PRODUCT_DEVICE/res_pack/m8b_dongle_ver1
cp $PRODUCT_OUT/meson8b_stvm8b_dongle_1G_v1.dtb $DIR/m8b_dongle_ver1
echo "make normal ver2 dtb for box camera"
./$PRODUCT_DEVICE/build_dtb.sh meson8b_stvm8b_box_1G_v2
cp $PRODUCT_OUT/meson8b_stvm8b_box_1G_v2.dtb $PRODUCT_DEVICE/res_pack/m8b_normal_ver2
cp $PRODUCT_OUT/meson8b_stvm8b_box_1G_v2.dtb $DIR/m8b_normal_ver2
fi


#make package
echo ############################################
rm -rf $PRODUCT_OUT/$FILE
rm -rf $PRODUCT_OUT/res-package.img
echo making otapackage ...
rm -rf $PRODUCT_OUT/obj/PACKAGING/target_files_intermediates
rm -rf $PRODUCT_OUT/system/app
rm -rf $PRODUCT_OUT/system/build.prop
make otapackage -j4
if [ ! -f "$PRODUCT_OUT/$FILE" ]; then 
	echo make package error!
	exit 1
fi

#make uboot otapackage
echo ############################################
echo making uboot otapackage ...
METAINFO=/tmp/mkspi/spi/META-INF/com/google/android/
mkdir -p $METAINFO
cp $PRODUCT_OUT/symbols/system/bin/updater $METAINFO/update-binary
SCRIPT_FILE=$METAINFO/updater-script
#echo "assert(!less_than_int(`date +%s`, getprop(\"ro.build.date.utc\")));" > $SCRIPT_FILE
echo "show_progress(0.900000, 15);" > $SCRIPT_FILE
echo "assert(getprop(\"ro.product.device\") == \"${TARGET_PRODUCT}\" || " >> $SCRIPT_FILE
echo "		getprop(\"ro.build.product\") == \"${TARGET_PRODUCT}\"); " >> $SCRIPT_FILE
echo "write_raw_image(package_extract_file(\"bootloader.img\"), \"bootloader\");">> $SCRIPT_FILE
echo "set_bootloader_env("upgrade_step", "1");">> $SCRIPT_FILE
cp $PRODUCT_OUT/u-boot.bin /tmp/mkspi/spi/bootloader.img
cd /tmp/mkspi/spi
zip -qr /tmp/mkspi/build.zip * 
cd $PRODUCT_ROOT
java -Xmx1536m -jar out/host/linux-x86/framework/signapk.jar -w build/target/product/security/testkey.x509.pem build/target/product/security/testkey.pk8 /tmp/mkspi/build.zip $PRODUCT_OUT/$SPI_FILE
rm -rf /tmp/mkspi


#copy
echo ############################################
echo making copy ...
repo manifest -o $DIR/manifest.xml -r --suppress-upstream-revision
cp $PRODUCT_OUT/u-boot.bin $DIR/u-boot.bin
mv $PRODUCT_OUT/$SPI_FILE $DIR/$SPI_FILE

cp $PRODUCT_DEVICE/res_pack/m8b_* $DIR
cp $PRODUCT_OUT/$FILE $DIR/$FILE1
cp $PRODUCT_OUT/boot.img $DIR/boot.img
cp $PRODUCT_OUT/recovery.img $DIR/recovery.img
cp $PRODUCT_OUT/aml_upgrade_package.img $DIR/aml_upgrade_package.img
cp $PRODUCT_OUT/obj/KERNEL_OBJ/vmlinux $DIR/vmlinux
cp $PRODUCT_OUT/obj/KERNEL_OBJ/System.map $DIR/System.map
echo "--wipe_data" >$DIR/factory_update_param.aml
echo "--wipe_cache" >>$DIR/factory_update_param.aml
echo "--wipe_media" >>$DIR/factory_update_param.aml
echo "--update_package=/sdcard/$FILE1" >>$DIR/factory_update_param.aml
