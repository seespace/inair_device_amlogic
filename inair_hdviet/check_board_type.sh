#!/system/bin/sh

if [ "$(getprop ubootenv.var.boardtype)" = "dongle" ]; then
    setprop hw.boardtype.dongle true
fi
