# Copyright (C) 2011 Amlogic Inc
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# This file is the build configuration for a full Android
# build for MX reference board. This cleanly combines a set of
# device-specific aspects (drivers) with a device-agnostic
# product configuration (apps).
#

# k200b:
# To build for k200b, 'export BOARD_REVISION=b' or 'b_2G' before make.
# k200a-2G:
# To build for k200a-2G, 'export BOARD_REVISION=a_2G' before make.

# Inherit from those products. Most specific first.
$(call inherit-product-if-exists, vendor/google/products/gms.mk)
$(call inherit-product, device/amlogic/common/mbx_amlogic.mk)
#$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base.mk)

# Replace definitions used by tablet in mid_amlogic.mk above
# Overrides
PRODUCT_BRAND := SeeSpace
PRODUCT_DEVICE := Android Reference Device
PRODUCT_NAME := Android Reference Design
PRODUCT_CHARACTERISTICS := tablet

include frameworks/native/build/mbox-1024-dalvik-heap.mk

# Discard inherited values and use our own instead.
PRODUCT_NAME := inair_hdviet
PRODUCT_MANUFACTURER := SeeSpace
PRODUCT_DEVICE := inair_hdviet
PRODUCT_MODEL := InAiR HDViet
# PRODUCT_CHARACTERISTICS := tablet,nosdcard
# export BUILD_ID=$(shell sed -n '1p' device/amlogic/android.log)$(shell sed -n '1p' device/amlogic/inair_hdviet/android.log)_$(shell date +%Y%m%d)
TARGET_SUPPORT_USB_BURNING_V2 := true
ifeq ($(TARGET_SUPPORT_USB_BURNING_V2),true)
TARGET_USB_BURNING_V2_DEPEND_MODULES := img-package
#BOARD_USERDATAIMAGE_PARTITION_SIZE := 12884901888
#BOARD_CACHEIMAGE_PARTITION_SIZE := 367001600
#BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := ext4
endif

#framebuffer use 3 buffers
TARGET_USE_TRIPLE_FB_BUFFERS := true

BOARD_USES_AML_SENSOR_HAL := true

#########################################################################
#
#                                                Audio
#
#########################################################################
PRODUCT_PROPERTY_OVERRIDES += \
       ro.camera.orientation.front=270 \
       ro.camera.orientation.back=0

#possible options: 1 tiny 2 legacy
BOARD_ALSA_AUDIO := tiny
BOARD_AUDIO_CODEC := dummy
BOARD_USE_USB_AUDIO := true

ifneq ($(strip $(wildcard $(LOCAL_PATH)/mixer_paths.xml)),)
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/mixer_paths.xml:system/etc/mixer_paths.xml
endif

include device/amlogic/common/audio.mk

ifeq ($(BOARD_ALSA_AUDIO),legacy)
PRODUCT_PROPERTY_OVERRIDES += \
    alsa.mixer.capture.master=Digital \
    alsa.mixer.capture.headset=Digital \
    alsa.mixer.capture.earpiece=Digital
endif

#########################################################################
#
#                                                USB
#
#########################################################################

BOARD_USES_USB_PM := true

#########################################################################
#
#                                                WiFi
#
#########################################################################

#WIFI_MODULE := rtl8192cu
#WIFI_MODULE := rtl8188eu
WIFI_MODULE := all
#WIFI_AP6xxx_MODULE := AP6330
include device/amlogic/common/wifi.mk

# Change this to match target country
# 11 North America; 14 Japan; 13 rest of world
PRODUCT_DEFAULT_WIFI_CHANNELS := 11



#########################################################################
#
#                                               Bluetooth
#
#########################################################################

BOARD_HAVE_BLUETOOTH := true
BLUETOOTH_MODULE := ALL

include device/amlogic/common/bluetooth.mk

#########################################################################
#
#                                                GPS
#
#########################################################################

GPS_MODULE :=
include device/amlogic/common/gps.mk



#########################################################################
#
#                                                Init.rc
#
#########################################################################

PRODUCT_COPY_FILES += \
	device/amlogic/common/init/mbx/init.amlogic.rc:root/init.amlogic.rc \
	$(LOCAL_PATH)/init.amlogic.usb.rc:root/init.amlogic.usb.rc \
	device/amlogic/common/init/mbx/ueventd.amlogic.rc:root/ueventd.amlogic.rc

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/init.amlogic.board.rc:root/init.amlogic.board.rc

#########################################################################
#
#                                                languages
#
#########################################################################

# For all locales, $(call inherit-product, build/target/product/languages_full.mk)
PRODUCT_LOCALES := en_US fr_FR it_IT es_ES de_DE nl_NL cs_CZ pl_PL ja_JP zh_TW zh_CN ru_RU \
   ko_KR nb_NO es_US da_DK el_GR tr_TR pt_PT pt_BR rm_CH sv_SE bg_BG ca_ES en_GB fi_FI hi_IN \
   hr_HR hu_HU in_ID iw_IL lt_LT lv_LV ro_RO sk_SK sl_SI sr_RS uk_UA vi_VN tl_PH ar_EG fa_IR \
   th_TH sw_TZ ms_MY af_ZA zu_ZA am_ET hi_IN et_EE


#########################################################################
#
#                                                Software features
#
#########################################################################

BUILD_WITH_AMLOGIC_PLAYER := true
BUILD_WITH_APP_OPTIMIZATION := true
BUILD_WITH_WIDEVINE_DRM := true
#BUILD_WITH_PLAYREADY_DRM := true
BUILD_WITH_EREADER := false
BUILD_WITH_MIRACAST := false
BUILD_WITH_XIAOCONG := false
BUILD_WITH_THIRDPART_APK := false
BUILD_WITH_BOOT_PLAYER:= true
BUILD_AMVIDEO_CAPTURE_TEST:=false
ifeq ($(wildcard vendor/google/products/gms.mk),)
# facelock enable, board should have front camera
BUILD_WITH_FACE_UNLOCK := true
endif

include device/amlogic/common/software.mk

#########################################################################
#
#                                                Misc
#
#########################################################################


# The OpenGL ES API level that is natively supported by this device.
# This is a 16.16 fixed point number
PRODUCT_PROPERTY_OVERRIDES += \
	ro.opengles.version=131072


PRODUCT_PACKAGES += \
	AppInstaller \
	VideoPlayer \
	SubTitle \
	Samba \
	smbd\
	libsmbbase \
	libsmbmnt \
	RemoteIME \
	remotecfg \
	DLNA \
	Update \
	RC_Server \
	AmlMiracast \
	wpa_supplicant_realtek \
	Discovery.apk \
	PromptUser \
	libasound \
	alsalib-alsaconf \
	alsalib-pcmdefaultconf \
	alsalib-cardsaliasesconf \
	libamstreaming \
	bootplayer \
	lib_driver_load \
	libamvdec \
	libstagefright_httplive \
	libamthumbnail \
	libamplayer \
	libTVaudio \
  wpa_cli \

BUILD_WITH_PPPOE := false

ifeq ($(BUILD_WITH_PPPOE),true)
PRODUCT_PACKAGES += \
    libpppoejni \
    pppoe_wrapper \
    pppoe \
    amlogic.pppoe \
    amlogic.pppoe.xml
PRODUCT_PROPERTY_OVERRIDES += \
    ro.platform.has.pppoe=true
endif


# Device specific system feature description
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/tablet_core_hardware.xml:system/etc/permissions/tablet_core_hardware.xml \
	$(LOCAL_PATH)/Third_party_apk_camera.xml:system/etc/Third_party_apk_camera.xml \
	frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml \
	frameworks/native/data/etc/android.hardware.sensor.gyroscope.xml:system/etc/permissions/android.hardware.sensor.gyroscope.xml \
	frameworks/native/data/etc/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml


PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/alarm_blacklist.txt:/system/etc/alarm_blacklist.txt \
	$(LOCAL_PATH)/alarm_whitelist.txt:/system/etc/alarm_whitelist.txt \
	$(LOCAL_PATH)/remote.conf:system/etc/remote.conf \
	$(LOCAL_PATH)/default_shortcut.cfg:system/etc/default_shortcut.cfg \
	$(LOCAL_PATH)/Vendor_0681_Product_0756.kl:system/usr/keylayout/Vendor_0681_Product_0756.kl \
	$(LOCAL_PATH)/Vendor_0001_Product_0001.kl:system/usr/keylayout/Vendor_0001_Product_0001.kl \
	$(LOCAL_PATH)/Vendor_0416_Product_1010.kl:system/usr/keylayout/Vendor_0416_Product_1010.kl \
	$(LOCAL_PATH)/Vendor_0416_Product_1021.kl:system/usr/keylayout/Vendor_0416_Product_1021.kl \
	$(LOCAL_PATH)/Vendor_0f30_Product_0001.kl:system/usr/keylayout/Vendor_0f30_Product_0001.kl \
	$(LOCAL_PATH)/Vendor_0c45_Product_0103.kl:system/usr/keylayout/Vendor_0c45_Product_0103.kl \
	$(LOCAL_PATH)/Vendor_0c45_Product_7403.kl:system/usr/keylayout/Vendor_0c45_Product_7403.kl \
	$(LOCAL_PATH)/Vendor_0c45_Product_0302.kl:system/usr/keylayout/Vendor_0c45_Product_0302.kl \
	$(LOCAL_PATH)/Vendor_1915_Product_003b.kl:system/usr/keylayout/Vendor_1915_Product_003b.kl \
	$(LOCAL_PATH)/Vendor_04b4_Product_2402.kl:system/usr/keylayout/Vendor_04b4_Product_2402.kl \
	$(LOCAL_PATH)/Vendor_0c45_Product_1109.kl:system/usr/keylayout/Vendor_0c45_Product_1109.kl \
	$(LOCAL_PATH)/Vendor_1d57_Product_ad03.kl:system/usr/keylayout/Vendor_1d57_Product_ad03.kl \
	$(LOCAL_PATH)/Vendor_1915_Product_003b.kcm:system/usr/keychars/Vendor_1915_Product_003b.kcm \
	$(LOCAL_PATH)/Vendor_0c45_Product_0302.kcm:system/usr/keychars/Vendor_0c45_Product_0302.kcm


PRODUCT_COPY_FILES += \
	device/amlogic/common/res/screen_saver/dlna.jpg:system/media/screensaver/images/dlna.jpg \
	device/amlogic/common/res/screen_saver/miracast.jpg:system/media/screensaver/images/miracast.jpg \
	device/amlogic/common/res/screen_saver/phone_remote.jpg:system/media/screensaver/images/phone_remote.jpg

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/u-boot.bin:u-boot.bin

#low memory killer
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/lowmemorykiller.txt:/system/etc/lowmemorykiller.txt

# App optimization
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/liboptimization.so:system/lib/liboptimization.so \
	$(LOCAL_PATH)/config:system/etc/config  \
	$(LOCAL_PATH)/wifi/config.txt:system/etc/wifi/config.txt

# bootanimation and bootvideo
PRODUCT_COPY_FILES += \
  $(LOCAL_PATH)/bootanimation.zip:system/media/bootanimation.zip \
  $(LOCAL_PATH)/mbox.mp4:system/etc/bootvideo

#add rony for Miracast
PRODUCT_PROPERTY_OVERRIDES += persist.miracast.hdcp2=false
PRODUCT_PACKAGES += \
		libstagefright_hdcp \
		libstagefright_hdcpkey

#add rony for dongle
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/check_board_type.sh:/system/bin/check_board_type.sh

#add rony for internal camera,used "ubootenv.var.camera" prop
PRODUCT_PACKAGES += \
		camera_in.amlogic

$(call inherit-product, vendor/inair/product.mk)
$(call inherit-product, $(LOCAL_PATH)/version_id.mk)