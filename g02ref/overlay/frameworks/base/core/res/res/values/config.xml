<?xml version="1.0" encoding="utf-8"?>
<!--
/*
** Copyright 2009, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License"); 
** you may not use this file except in compliance with the License. 
** You may obtain a copy of the License at 
**
**     http://www.apache.org/licenses/LICENSE-2.0 
**
** Unless required by applicable law or agreed to in writing, software 
** distributed under the License is distributed on an "AS IS" BASIS, 
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
** See the License for the specific language governing permissions and 
** limitations under the License.
*/
-->

<!-- These resources are around just to allow their values to be customized
     for different hardware and product builds. -->
<resources xmlns:xliff="urn:oasis:names:tc:xliff:document:1.2">
    <!-- Flag indicating whether the surface flinger is inefficient
         at performing a blur.  Used by parts of the UI to turn off
         the blur effect where it isn't worth the performance hit. -->
    <bool name="config_sf_slowBlur">false</bool>
    <bool name="config_animateScreenLights">false</bool>
    <!-- Flag indicating whether the we should enable the automatic brightness in Settings.
         Software implementation will be used if config_hardware_auto_brightness_available is not set -->
    <bool name="config_automatic_brightness_available">false</bool>

    <!-- Array of light sensor LUX values to define our levels for auto backlight brightness support.
         The N entries of this array define N + 1 zones as follows:

         Zone 0:        0 <= LUX < array[0]
         Zone 1:        array[0] <= LUX < array[1]
         ...
         Zone N:        array[N - 1] <= LUX < array[N]
         Zone N + 1:    array[N] <= LUX < infinity

         Must be overridden in platform specific overlays -->
    <!-- these are the possible lux values as defined in the sensor hal for this device: 100,500,800,1500,3500.
         100 will be in zone 0, 500 will be in zone 1, and so on, according to the comment above. -->
    <integer-array name="config_autoBrightnessLevels">
        <item>500</item>
        <item>800</item>
        <item>1500</item>
        <item>3500</item>
    </integer-array>

    <!-- Array of output values for LCD backlight corresponding to the LUX values
         in the config_autoBrightnessLevels array.  This array should have size one greater
         than the size of the config_autoBrightnessLevels array.
         This must be overridden in platform specific overlays -->
    <integer-array name="config_autoBrightnessLcdBacklightValues">
        <item>20</item>
        <item>80</item>
        <item>120</item>
        <item>160</item>
        <item>250</item>
    </integer-array>

    <!-- Array of output values for button backlight corresponding to the LUX values
         in the config_autoBrightnessLevels array.  This array should have size one greater
         than the size of the config_autoBrightnessLevels array.
         This must be overridden in platform specific overlays -->
    <integer-array name="config_autoBrightnessButtonBacklightValues">
        <item>255</item>
        <item>255</item>
        <item>255</item>
        <item>255</item>
        <item>255</item>
    </integer-array>

    <!-- Array of output values for keyboard backlight corresponding to the LUX values
         in the config_autoBrightnessLevels array.  This array should have size one greater
         than the size of the config_autoBrightnessLevels array.
         This must be overridden in platform specific overlays -->
    <integer-array name="config_autoBrightnessKeyboardBacklightValues">
        <item>255</item>
        <item>255</item>
        <item>255</item>
        <item>255</item>
        <item>255</item>
    </integer-array>

    <!-- Amount of time it takes for the light sensor to warm up in milliseconds.
         For this time after the screen turns on, the Power Manager
         will not debounce light sensor readings -->
    <integer name="config_lightSensorWarmupTime">0</integer>

    <!-- Do not translate. Defines the slots for the right-hand side icons.  That is to say, the
         icons in the status bar that are not notifications. -->
    <string-array name="config_statusBarIcons">
       <item><xliff:g id="id">ime</xliff:g></item>
       <item><xliff:g id="id">sync_failing</xliff:g></item>
       <item><xliff:g id="id">sync_active</xliff:g></item>
       <item><xliff:g id="id">gps</xliff:g></item>
       <item><xliff:g id="id">bluetooth</xliff:g></item>
       <item><xliff:g id="id">nfc</xliff:g></item>
       <item><xliff:g id="id">tty</xliff:g></item>
       <item><xliff:g id="id">speakerphone</xliff:g></item>
       <item><xliff:g id="id">mute</xliff:g></item>
       <item><xliff:g id="id">volume</xliff:g></item>
       <item><xliff:g id="id">wifi</xliff:g></item>
       <item><xliff:g id="id">ethernet</xliff:g></item>
       <item><xliff:g id="id">cdma_eri</xliff:g></item>
       <item><xliff:g id="id">data_connection</xliff:g></item>
       <item><xliff:g id="id">phone_evdo_signal</xliff:g></item>
       <item><xliff:g id="id">phone_signal</xliff:g></item>
       <item><xliff:g id="id">battery</xliff:g></item>
       <item><xliff:g id="id">alarm_clock</xliff:g></item>
       <item><xliff:g id="id">secure</xliff:g></item>
       <item><xliff:g id="id">clock</xliff:g></item>
    </string-array>

    <!-- This string array should be overridden by the device to present a list of network
         attributes.  This is used by the connectivity manager to decide which networks can coexist
         based on the hardware -->
    <!-- An Array of "[Connection name],[ConnectivityManager connection type],
         [associated radio-type],[priority]  -->
    <string-array translatable="false" name="networkAttributes">
        <item>"wifi,1,1,1,-1,true"</item>
        <item>"mobile,0,0,0,-1,true"</item>
<!--
        <item>"mobile_mms,2,0,2,60000,true"</item>
        <item>"mobile_supl,3,0,2,60000,true"</item>
        <item>"mobile_hipri,5,0,3,60000,true"</item>
        <item>"mobile_fota,10,0,2,60000,true"</item>
        <item>"mobile_ims,11,0,2,60000,true"</item>
        <item>"mobile_cbs,12,0,2,60000,true"</item>
-->
        <item>"wifi_p2p,13,1,0,-1,true"</item>
        <item>"ethernet,9,9,1,-1,true"</item>
	<item>"pppoe,14,14,1,-1,true"</item>
    </string-array>

    <!-- This string array should be overridden by the device to present a list of radio
         attributes.  This is used by the connectivity manager to decide which networks can coexist
         based on the hardware -->
    <!-- An Array of "[ConnectivityManager connectionType],
                      [# simultaneous connection types]"  -->
    <string-array translatable="false" name="radioAttributes">
        <item>"1,1"</item>
        <item>"0,1"</item>
        <item>"9,1"</item>
        <item>"14,1"</item>
    </string-array>
    <!-- List of regexpressions describing the interface (if any) that represent tetherable
         Wifi interfaces.  If the device doesn't want to support tethering over Wifi this
         should be empty.  An example would be "softap.*" -->
    <string-array translatable="false" name="config_tether_wifi_regexs">
        <item>"wlan0"</item>
    </string-array>    
    <!-- Array of ConnectivityManager.TYPE_xxxx values allowable for tethering -->
    <!-- Common options are [1, 4] for TYPE_WIFI and TYPE_MOBILE_DUN or
    <!== [0,1,5,7] for TYPE_MOBILE, TYPE_WIFI, TYPE_MOBILE_HIPRI and TYPE_BLUETOOTH -->
    <integer-array translatable="false" name="config_tether_upstream_types">
        <item>0</item>
        <item>1</item>
        <item>9</item>
        <item>14</item>
    </integer-array>

    <!-- Set and Unsets Ethernet -->
    <!--<bool name="config_ethernetEnabled">true</bool>-->

	<!-- Flag indicating whether the current device is "voice capable".
         If true, this means that the device supports circuit-switched
         (i.e. voice) phone calls over the telephony network, and is
         allowed to display the in-call UI while a cellular voice call is
         active.  This can be overridden to false for "data only" devices
         which can't make voice calls and don't support any in-call UI.

         Note: this flag is subtly different from the
         PackageManager.FEATURE_TELEPHONY system feature, which is
         available on *any* device with a telephony radio, even if the
         device is data-only. -->
    <bool name="config_voice_capable">false</bool>

    <!-- Enables or disables fading edges when marquee is enabled in TextView. -->
    <bool name="config_ui_enableFadingMarquee">false</bool>

    <!-- Whether a software navigation bar should be shown. NOTE: in the future this may be
         autodetected from the Configuration. -->
    <bool name="config_showNavigationBar">true</bool>
    
    <!-- Wifi driver stop delay, in milliseconds.
         Default value is 1 second. -->
    <integer translatable="false" name="config_wifi_driver_stop_delay">0</integer>
    <!-- The maximum duration (in milliseconds) we expect a network transition to take -->
    <integer name="config_networkTransitionTimeout">0</integer>
	
    <!-- Default wallpaper width and height -->
    <!--integer name="config_defaultWallPaper_width">1990</integer-->
    <!--integer name="config_defaultWallPaper_height">1280</integer-->
    <integer name="config_defaultWallPaper_width">1990</integer>
    <integer name="config_defaultWallPaper_height">1280</integer>

        <!-- Component name of the service providing network location support. -->
    <!--string name="config_networkLocationProviderPackageName">com.google.android.location</string -->

    <!-- Component name of the service providing geocoder API support. -->
    <!-- string name="config_geocodeProviderPackageName">com.google.android.location</string -->
    <integer name="config_safe_media_volume_index">10</integer>

    <!-- Whether WiFi display is supported by this device.
         There are many prerequisites for this feature to work correctly.
         Here are a few of them:
         * The WiFi radio must support WiFi P2P.
         * The WiFi radio must support concurrent connections to the WiFi display and
           to an access point.
         * The Audio Flinger audio_policy.conf file must specify a rule for the "r_submix"
           remote submix module.  This module is used to record and stream system
           audio output to the WiFi display encoder in the media server.
         * The remote submix module "audio.r_submix.default" must be installed on the device.
         * The device must be provisioned with HDCP keys (for protected content).
    -->
    <bool name="config_enableWifiDisplay">true</bool>
</resources>
